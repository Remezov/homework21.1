package ru.remezov.toLeft;

public class App {
    public static void main(String[] args) {
        int count1 = 10;
        int count2 = 10;
        int[][] array = new int[count1][count2];

        for (int i = 0; i < count1; i++) {
            System.out.println();
            for (int j = 0; j < count2; j++) {
                array[i][j] = j;
                System.out.print(array[i][j]+" ");
            }
        }

        System.out.println();
        toLeft(array,count1,count2);
    }

    public static void toLeft(int[][] array, int count1, int count2) {
        for (int i = 0; i < count1; i++) {
            System.out.println();
            for (int j = 0; j < count2; j++) {
                if (j == count2 -1) {
                    array[i][j] = 0;
                } else {
                    array[i][j] = array[i][j + 1];
                }
                System.out.print(array[i][j] + " ");
            }
        }
    }
}
